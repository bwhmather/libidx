LibIDX
======

|build-status| |coverage|

A C library for reading and writing files in the IDX format, as used for the `MNIST data-set`_.


License
-------

The project is made available under the MIT license.  See `LICENSE`_ for details.


.. |build-status| image:: https://travis-ci.org/bwhmather/libidx.png?branch=develop
    :target: https://travis-ci.org/bwhmather/libidx
    :alt: Build Status
.. |coverage| image:: https://coveralls.io/repos/bwhmather/libidx/badge.png?branch=develop
    :target: https://coveralls.io/r/bwhmather/libidx?branch=develop
    :alt: Coverage
.. _MNIST data-set: http://yann.lecun.com/exdb/mnist/
.. _LICENSE: ./LICENSE
